<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Request;
use think\facade\Log;
// 应用公共文件

!defined('DS') && define('DS', DIRECTORY_SEPARATOR);

// 运行目录
define('ROOT_URL', ltrim(dirname(Request::rootUrl()), DS) . '/');


/**
 * 递归树状结构
 * @param $arrs
 * @param $category
 * @param int $parent_id
 * @return mixed
 */
function recur($arrs,$category,$parent_id = 0,$num,$level)
{
    $level++;
    if($num!=-1){
        if($num<$level){
            return;
        }
    }
    foreach ($category as $k => $v) {
        if($v['parent_id'] == $parent_id)
        {
            $arr = array('id'=>$v['id'],'text'=>$v['category_name'],'tree_names'=>$v['tree_names'],'level'=>$level,'brand_id'=>$v['brand_id'],'tree_path'=>$v['tree_path'],'number'=>$v['category_number'],'children'=>array());
            $arr['children'] = recur($arr['children'],$category,$v['id'],$num,$level);
            array_push($arrs,$arr);
        }
    }
    return $arrs;
}