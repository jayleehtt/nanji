<?php
/**
 * Created by PhpStorm.
 * User: jayleehtt
 * Date: 2020/6/6
 * Time: 14:47
 */

namespace app\admin\model;


use app\core\model\BaseModel;
use think\Db;

class GoodsCategory extends BaseModel
{
    public function getCateByBrandId($brandId){
        $categories = Db::name("goods_category")
            ->alias('t1')
            ->join('category_brand t2','t1.id = t2.category_id')
            ->where("t2.brand_id={$brandId}")
            ->select();
        $cateIdArr=array();
        foreach ($categories as $cate){
            $ids=explode(",",rtrim($cate['tree_path'],","));
            foreach ($ids as $id){
                array_push($cateIdArr,$id);
            }
        }
        $cateIdArr=array_unique($cateIdArr);
        if(count($cateIdArr)>0){
            $cateIdStr = implode(",",$cateIdArr);
            $categories = Db::query("SELECT t1.id,t1.parent_id,t1.category_number,t1.tree_names,t1.tree_path,t1.category_name,t2.brand_id as brand_id FROM fo_goods_category t1 LEFT JOIN fo_category_brand t2 on t1.id=t2.category_id WHERE  t1.id IN ($cateIdStr) order by t1.category_number asc ");
            return $categories;
        }else{
            return [];
        }
    }

}