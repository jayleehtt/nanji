<?php
/**
 * Created by PhpStorm.
 * User: VSZ_005
 * Date: 2020/6/5
 * Time: 16:51
 */

namespace app\admin\controller;


use app\core\controller\BaseCtrl;
use think\Db;
use think\facade\Env;
use think\facade\Log;
use app\admin\model\Goods as GoodsModel;
use think\Request;

/**
 * 品牌管理
 * Class Brand
 * @package app\admin\controller
 */
class Goods extends BaseCtrl
{
    public function create()
    {
        /*$brand = new BrandModel;
        $brand->save([
            'brand_name' => 'GGG'
        ]);*/

        $this->fetch();
    }


    public function table()
    {
        return $this->fetch();
    }


    public function import()
    {
        //上传excel文件
        $file = request()->file('file');
        //var_dump($file);
        //exit();
        //将文件保存到public/uploads目录下面1297920
        $info = $file->validate(['size' => 104857600, 'ext' => 'xls,xlsx'])->move('./uploads');
        if ($info) {
            //获取上传到后台的文件名
            $fileName = $info->getSaveName();
            //获取文件路径
            $filePath = Env::get('root_path') . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $fileName;
            //获取文件后缀
            $suffix = $info->getExtension();
            require '../extend/PHPExcel/PHPExcel.php';
            //判断哪种类型
            if ($suffix == "xlsx") {
                $reader = \PHPExcel_IOFactory::createReader('Excel2007');
            } else {
                $reader = \PHPExcel_IOFactory::createReader('Excel5');
            }
        } else {
            $this->error('文件过大或格式不正确导致上传失败-_-!');
        }
        //载入excel文件
        $excel = $reader->load("$filePath", $encode = 'utf-8');
        $sheet = $excel->getsheet(0);//->toArray();   //转换为数组格式
        //array_shift($excel_array);  //删除第一个数组(标题);
        //var_dump($excel_array);
        //exit();
        //$maxCol = $sheet->getHighestColumn();
        //$maxRow = $sheet->getHighestRow();

        //获取总行数
        $row_num = $sheet->getHighestRow();
        //获取总列数
        $col_num = $sheet->getHighestColumn();


        $goods = []; //数组形式获取表格数据
        $data = []; //数组形式获取表格数据
        for ($i = 2; $i <= $row_num; $i++) {
            $data['id'] = $sheet->getCell("A" . $i)->getValue();
            $data['create_at'] = $sheet->getCell("B" . $i)->getValue();
            $data['update_at'] = $excel->getActiveSheet()->getCell("C" . $i)->getValue();
            $data['create_by'] = $excel->getActiveSheet()->getCell("D" . $i)->getValue();
            $data['update_by'] = $excel->getActiveSheet()->getCell("E" . $i)->getValue();
            $data['goods_name'] = $excel->getActiveSheet()->getCell("F" . $i)->getValue();
            $data['goods_number'] = $excel->getActiveSheet()->getCell("G" . $i)->getValue();
            $data['goods_specification'] = $excel->getActiveSheet()->getCell("H" . $i)->getValue();
            $data['price_mode_id'] = $excel->getActiveSheet()->getCell("I" . $i)->getValue();
            $data['category_id'] = $excel->getActiveSheet()->getCell("J" . $i)->getValue();
            $data['brand_id'] = $excel->getActiveSheet()->getCell("K" . $i)->getValue();
            $data['unit_id'] = $excel->getActiveSheet()->getCell("L" . $i)->getValue();
            $data['created_date'] = $excel->getActiveSheet()->getCell("M" . $i)->getValue();
            $data['standard_price'] = $excel->getActiveSheet()->getCell("N" . $i)->getValue();
            $data['is_auxiliary'] = $excel->getActiveSheet()->getCell("O" . $i)->getValue();
            array_push($goods, $data);
            //将数据保存到数据库
        }


        //var_dump($goods);
        //exit();
        $chunk_result = array_chunk($goods, 1000);
        // 启动事务
        Db::startTrans();
        try {
            Db::name('goods')->delete(true);
            foreach ($chunk_result as $new_list) {
                //此处批量插入数据操作
                $res = Db::name('goods')->insertAll($new_list);
            }
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            var_dump($e);
            // 回滚事务
            Db::rollback();
        }

        echo 1;
        //return $this->fetch();
    }


    public function pageList()
    {
        $page = input('page');
        $rows = input('rows');
        if (empty($page)) $page = 1;
        if (empty($rows)) $rows = 10;
        $list = Db::name("goods")
            ->alias("g")
            ->field("g.id,g.goods_number,g.is_auxiliary,g.goods_name,g.goods_specification,g.standard_price,gc.tree_names,b.brand_name,u.unit_name")
            ->leftJoin("unit u", "g.unit_id=u.id")
            ->leftJoin("fo_goods_category gc", "g.category_id=gc.id")
            ->leftJoin("fo_brand b", "g.brand_id=b.id");

        $goods_number = input('goods_number');
        if (!empty($goods_number)) {
            $list->where("g.goods_number like '%{$goods_number}%'");
        }

        $goods_name = input('goods_name');
        if (!empty($goods_name)) {
            $list->where("g.goods_name like '%{$goods_name}%'");
        }

        $goods_specification = input('goods_specification');
        if (!empty($goods_specification)) {
            $list->where("g.goods_specification like '%{$goods_specification}%'");
        }


        $category_id = input('category_id');
        if (!empty($category_id)) {
            $list->where([
                "g.category_id" => explode(",", $category_id)
            ]);
        }

        $brand_id = input('brand_id');
        if (!empty($brand_id) && $brand_id != -1) {
            $list->where("g.brand_id = '{$brand_id}'");
        }


        $count = $list->count();
        $list = $list->page($page, $rows)->select();//获取列表


        /*$offset  = ($page-1)*$rows;
        $field="t1.id,t1.goods_number,t1.is_auxiliary,t1.goods_name,t1.goods_specification,t1.standard_price,t2.tree_names,t3.brand_name";
        $c="count(1) as count";
        $sql="select %s from fo_goods t1 LEFT JOIN fo_goods_category t2 on t1.category_id=t2.id LEFT JOIN fo_brand t3 on t1.brand_id=t3.id WHERE 1=1";
        $goods_number = input('goods_number');
        if(!empty($goods_number)){
            $sql.=" and t1.goods_number like '%{$goods_number}%'";
        }
        $goods_name = input('goods_name');
        if(!empty($goods_name)){
            $sql.=" and t1.goods_name like '%{$goods_name}%'";
        }
        $goods_specification = input('goods_specification');
        if(!empty($goods_specification)){
            $sql.=" and t1.goods_specification like '%{$goods_specification}%'";
        }

        $category_id = input('category_id');
        if(!empty($category_id)){
            $sql.=" and t1.category_id in($category_id)";
        }

        $brand_id = input('brand_id');
        if(!empty($brand_id)&&$brand_id!=-1){
            $sql.=" and t1.brand_id = '{$brand_id}'";
        }
        $count_sql=str_replace("%s",$c,$sql);
        $count = Db::query($count_sql);
        $total =  $count[0]["count"];
        $sql.=" order  by t1.update_at desc LIMIT {$offset},{$rows}";
        $data_sql=str_replace("%s",$field,$sql);
        Log::record("货品SQL：".$data_sql,"info");
        $rows = Db::query($data_sql);*/
        $res = array(
            'total' => $count,
            'rows' => $list
        );
        return json($res);
    }


    public function all()
    {
        $list = Db::name("goods")
            ->alias("g")
            ->field("g.*,u.unit_name")
            ->leftJoin("unit u", "g.unit_id=u.id");

        $goods_number = input('goods_number');
        if (!empty($goods_number)) {
            $list->where("goods_number like '%{$goods_number}%'");
        }

        $goods_name = input('goods_name');
        if (!empty($goods_name)) {
            $list->where("goods_name like '%{$goods_name}%'");
        }

        $list = $list->select();//获取列表

        $res = array(
            'total' => 0,
            'rows' => $list
        );
        return json($res);

    }


    public function page_list()
    {
        $page = input('page');
        $rows = input('rows');
        $brand_id = input("brandId");
        $cidsStr = input("cids");
        $cids = explode(",", $cidsStr);
        if (empty($page)) $page = 1;
        if (empty($rows)) $rows = 10;
        $list = Db::name("goods")
            ->alias("g")
            //->leftJoin("goods_category gc","g.category_id=gc.id")
            //->leftJoin("brand b","g.brand_id=b.id")
            ->where([
                "brand_id" => $brand_id,
                "category_id" => $cids
            ]);
        $count = $list->count("id");
        $list = $list->page($page, $rows)->select();//获取列表
        $res = array(
            'total' => $count,
            'rows' => $list
        );
        return json($res);
    }


    public function get($id)
    {
        $goods = Db::name("goods")
            ->alias("g")
            ->field("g.*,u.unit_name,gc.category_name,b.brand_name")
            ->leftJoin("unit u", "g.unit_id=u.id")
            ->leftJoin("goods_category gc", "g.category_id=gc.id")
            ->leftJoin("brand b", "g.brand_id=b.id")
            ->find(['g.id' => $id]);
        return json($goods);
    }


    public function update(Request $request)
    {
        //$goodModel = new GoodsModel;
        $id = input("id");
        $is_auxiliary = input("is_auxiliary");
        $standard_price = input("standard_price");
        $goods_number = input("goods_number");


        $good = Db::name("goods")
            ->where("id", $id)
            ->find();



//-----------------------------------------------




        // 启动事务
       Db::startTrans();
        try {
            Db::name("goods")
                ->where("id", $id)
                ->update([
                    "standard_price" => $standard_price,
                    "is_auxiliary" => $is_auxiliary,
                ]);


            if ($good["is_auxiliary"] == 0 && $is_auxiliary == 1) {//主材=====>辅材

                /*$cpp = Db::name("customer_price_policy")
                    ->alias("cpp")
                    ->field("pg.goods_number")
                    ->join("policy_goods pg","cpp.id=pg.policy_id")
                    ->where([
                        "is_auxiliary" => 0,
                        "pg.goods_number"=>$goods_number
                    ])
                    ->select();

                $gns=[];
                foreach ($cpp as $p){
                    array_push($gns,$p["goods_number"]);
                }
                Db::name("policy_goods")
                    ->where([
                        "goods_number"=>$gns
                    ])
                    ->delete(true);*/

                $policies = Db::name("customer_price_policy")
                    ->where([
                        "is_auxiliary" => 1
                    ])
                    ->select();

                $policyGoods=[];
                foreach ($policies as $p) {
                    $pd = [
                        "policy_id" => $p["id"],
                        "goods_number" => $goods_number,
                        "customer_number" => $p["customer_number"],
                        "is_auxiliary" => 1,
                    ];
                    array_push($policyGoods, $pd);
                }

                $ret = Db::name('policy_goods')->insertAll($policyGoods);

            } elseif ($good["is_auxiliary"] == 1 && $is_auxiliary == 0) {//辅材=====>主材


                /*$cpp = Db::name("customer_price_policy")
                    ->alias("cpp")
                    ->field("pg.goods_number,cpp.id")
                    ->join("policy_goods pg","cpp.id=pg.policy_id")
                    ->where([
                        "is_auxiliary" => 1,
                        "pg.goods_number"=>$goods_number
                    ])
                    ->select();


                $gns=[];
                foreach ($cpp as $p){
                    array_push($gns,$p["goods_number"]);
                }*/

                Db::name("policy_goods")
                    ->where([
                        "goods_number"=>$goods_number,
                        "is_auxiliary" => 1,
                    ])
                    ->delete(true);

                /*$policies = Db::name("customer_price_policy")
                    ->where([
                        "is_auxiliary" => 0
                    ])
                    ->select();

                $policyGoods=[];
                foreach ($policies as $p) {
                    $pd = [
                        "policy_id" => $p["id"],
                        "goods_number" => $goods_number,
                        "customer_number" => $p["customer_number"],
                    ];
                    array_push($policyGoods, $pd);
                }

                $ret = Db::name('policy_goods')->insertAll($policyGoods);*/
            }
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
        }



        $res = array(
            'errcode' => 0,
            'errmsg' => "ok"
        );

        return json($res);
       /* if ($ret) {

        } else {
            $res["errcode"] = 40000;
            $res["errmsg"] = "新增失败";
            return json($res);
        }*/
    }

}