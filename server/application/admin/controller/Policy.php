<?php
/**
 * Created by PhpStorm.
 * User: jayleehtt
 * Date: 2020/6/14
 * Time: 15:18
 */

namespace app\admin\controller;


use app\core\controller\BaseCtrl;
use app\admin\model\Brand;
use Think\Db;
use think\facade\Log;
use PHPExcel;//tp5.1用法
use PHPExcel_IOFactory;

class Policy extends BaseCtrl
{
    public function index()
    {
        //$b=new Brand();
        //$brands = $b->select();
        //->where('status',1)
        $brands = Db::name('brand')->select();
        $this->assign('brands', $brands);
        //Log::record("brands json：".json_encode($brands,JSON_UNESCAPED_UNICODE),"info");
        //$opts = json_decode($brands->getContent(),true);
        // $this->assign('opts', $opts);
        //$brands_json=$brands->toJson();

        return $this->fetch('index');
    }

    public function cat()
    {
        return $this->fetch();
    }

    public function policyBrand()
    {
        return $this->fetch("policy_brand");
    }

    public function policyAuxiliary()
    {
        return $this->fetch("auxiliary_brand");
    }

    public function policyManager()
    {
        return $this->fetch("policy_manage");
    }

    public function add_remove()
    {
        $data = request()->put();

        //var_dump($data);
        //exit(0);

        //查询客户
        $customers = Db::name("customer")->where("id={$data['customer']['id']} or path like '{$data['customer']['path']}%'")->select();


        Db::startTrans();
        try {
            if (count($data["remove"]) > 0) {
                foreach ($customers as $customer) {
                    if ($data["is_auxiliary"] == 0) {
                        $oldP = Db::name('customer_price_policy')->where([
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => $data["is_auxiliary"],
                            "brand_id" => $data['brandId'],
                        ])->find();

                    } elseif ($data["is_auxiliary"] == 1) {
                        $oldP = Db::name('customer_price_policy')->where([
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => $data["is_auxiliary"],
                        ])->find();
                    }

                    foreach ($data["remove"] as $policy) {

                        Db::name('policy_goods')->where([
                            "goods_number" => $policy["goods_number"],
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => $data["is_auxiliary"],
                            "policy_id" => $oldP["id"]
                        ])->delete(true);
                    }

                }
            }

            $goods = $data["add"];

            if (count($goods) > 0) {
                foreach ($customers as $customer) {
                    if ($data["is_auxiliary"] == 0) {
                        $oldP = Db::name('customer_price_policy')->where([
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => $data["is_auxiliary"],
                            "brand_id" => $data['brandId'],
                        ])->find();
                    } elseif ($data["is_auxiliary"] == 1) {
                        $oldP = Db::name('customer_price_policy')->where([
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => $data["is_auxiliary"],
                        ])->find();
                    }

                    $pds = [];
                    if (!empty($oldP)) {
                        foreach ($goods as $good) {
                            $pd["customer_number"] = $customer["customer_number"];
                            $pd["goods_number"] = $good["goods_number"];
                            $pd["policy_id"] = $oldP['id'];
                            $pd["is_auxiliary"] = $data["is_auxiliary"];
                            array_push($pds, $pd);
                        }

                        if (count($pds) > 0) {
                            $res = Db::name('policy_goods')->insertAll($pds);
                        }
                    } else {
                        $policy["create_at"] = date('Y-m-d H:i:s');
                        $policy["update_at"] = date('Y-m-d H:i:s');
                        $policy["create_by"] = 1;
                        $policy["update_by"] = 1;
                        $policy["customer_number"] = $customer["customer_number"];
                        //$policy["goods_number"] = $good["goods_number"];
                        //$policy["customer_name"] = $customer["customer_name"];
                        $policy["status"] = 1;
                        if ($data["is_auxiliary"] == 0) {
                            $policy["brand_id"] = $data['brandId'];
                        } elseif ($data["is_auxiliary"] == 1) {
                            $policy["brand_id"] = null;
                        }

                        $policy["discount"] = $data['discount'];
                        $policy["payback_discount"] = $data['paybackDiscount'];
                        $policy["payback"] = $data['payback'];
                        //$policy["category_id"] = $data['cid'];
                        $policy["is_auxiliary"] = $data["is_auxiliary"];
                        //$policy["policy_name"] = $customer["customer_name"] . "-" . $p['brandName'] . "-" . $p['cateName'];
                        //array_push($policies, $policy);
                        $policyId = Db::name('customer_price_policy')->insertGetId($policy);

                        foreach ($goods as $good) {
                            $policyGoods = [];
                            $pd = [
                                "policy_id" => $policyId,
                                "goods_number" => $good["goods_number"],
                                "customer_number" => $customer["customer_number"],
                                "is_auxiliary" => $data["is_auxiliary"]
                            ];
                            array_push($policyGoods, $pd);
                            $res = Db::name('policy_goods')->insertAll($policyGoods);
                        }

                    }
                }
            }
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
        }


        $res = array(
            "errcode" => "1",
            "errmsg" => "执行成功"
        );
        return json($res);
    }


    public function create()
    {

        $data = request()->put();

        //查询客户
        $customers = Db::name("customer")->where("id={$data['customer']['id']} or path like '{$data['customer']['path']}%'")->select();
        $policies = [];
        //品牌政策
        foreach ($customers as $customer) {
            foreach ($data["goods"] as $good) {
                $policy["create_at"] = date('Y-m-d H:i:s');
                $policy["update_at"] = date('Y-m-d H:i:s');
                $policy["create_by"] = 1;
                $policy["update_by"] = 1;
                $policy["customer_number"] = $customer["customer_number"];
                $policy["goods_number"] = $good["goods_number"];
                //$policy["customer_name"] = $customer["customer_name"];
                $policy["status"] = 1;
                //$policy["brand_id"] = $data['brandId'];
                $policy["brand_discount"] = $data['discount'];
                $policy["payback"] = $data['payback'];
                //$policy["category_id"] = $data['cid'];
                //$policy["is_auxiliary"] = 0;
                //$policy["policy_name"] = $customer["customer_name"] . "-" . $p['brandName'] . "-" . $p['cateName'];
                array_push($policies, $policy);
            }
        }
        Db::name('customer_price_policy')->insertAll($policies);
        return json(1);
    }


    public function brand_create()
    {

        set_time_limit(0);
        $data = request()->put();

        //查询客户
        $customers = Db::name("customer")->where("id={$data['customer']['id']} or path like '{$data['customer']['path']}%'")->select();


        $cnumbers = [];
        //判断是品牌折扣还是辅材折扣
        if ($data["is_auxiliary"] == 0) {
            if ($data["all"] == 0) { //全选  and is_auxiliary={$data["is_auxiliary"]}
                Db::startTrans();
                try {
                    $goods = Db::name("goods")->where("brand_id={$data["brandId"]}")->select();
                    foreach ($customers as $customer) {
                        array_push($cnumbers, $customer["customer_number"]);
                        $policy["create_at"] = date('Y-m-d H:i:s');
                        $policy["update_at"] = date('Y-m-d H:i:s');
                        $policy["create_by"] = 1;
                        $policy["update_by"] = 1;
                        $policy["customer_number"] = $customer["customer_number"];
                        //$policy["goods_number"] = $good["goods_number"];
                        //$policy["customer_name"] = $customer["customer_name"];
                        $policy["status"] = 1;
                        $policy["brand_id"] = $data['brandId'];
                        $policy["discount"] = $data['discount'];
                        $policy["payback"] = $data['payback'];
                        $policy["payback_discount"] = $data['paybackDiscount'];
                        //$policy["category_id"] = $data['cid'];
                        $policy["is_auxiliary"] = 0;


                        $oldP = Db::name('customer_price_policy')->where([
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => 0,
                            "brand_id" => $data['brandId'],
                        ])->find();


                        if (!empty($oldP)) {
                            //删除中间表
                            Db::name('policy_goods')->where([
                                "policy_id" => $oldP["id"]
                            ])->delete(true);


                            //删除政策
                            Db::name('customer_price_policy')->where([
                                "customer_number" => $customer["customer_number"],
                                "is_auxiliary" => 0,
                                "brand_id" => $data['brandId'],
                            ])->delete(true);
                        }


                        $policyId = Db::name('customer_price_policy')->insertGetId($policy);

                        $policyGoods = [];
                        foreach ($goods as $good) {
                            $pd = [
                                "policy_id" => $policyId,
                                "goods_number" => $good["goods_number"],
                                "customer_number" => $customer["customer_number"],
                                "is_auxiliary" => 0,
                            ];
                            array_push($policyGoods, $pd);
                        }
                        $res = Db::name('policy_goods')->insertAll($policyGoods);
                    }
                    // 提交事务
                    Db::commit();
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                }
            } elseif ($data["all"] == 1) { //全不选

                Db::startTrans();
                try {
                    foreach ($customers as $customer) {

                        $oldP = Db::name('customer_price_policy')->where([
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => 0,
                            "brand_id" => $data['brandId'],
                        ])->find();


                        if (!empty($oldP)) {
                            //删除中间表
                            Db::name('policy_goods')->where([
                                "policy_id" => $oldP["id"]
                            ])->delete(true);


                            //删除政策
                            Db::name('customer_price_policy')->where([
                                "customer_number" => $customer["customer_number"],
                                "is_auxiliary" => 0,
                                "brand_id" => $data['brandId'],
                            ])->delete(true);
                        }
                    }
                    // 提交事务
                    Db::commit();
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                }


            }
        } else {
            if ($data["all"] == 0) { //全选
                $goods = Db::name("goods")->where("is_auxiliary={$data["is_auxiliary"]}")->select();
                // 启动事务
                Db::startTrans();
                try {
                    foreach ($customers as $customer) {
                        $policy["create_at"] = date('Y-m-d H:i:s');
                        $policy["update_at"] = date('Y-m-d H:i:s');
                        $policy["create_by"] = 1;
                        $policy["update_by"] = 1;
                        $policy["customer_number"] = $customer["customer_number"];
                        //$policy["goods_number"] = $good["goods_number"];
                        //$policy["customer_name"] = $customer["customer_name"];
                        $policy["status"] = 1;
                        //$policy["brand_id"] = $data['brandId'];
                        $policy["discount"] = $data['discount'];
                        $policy["payback"] = $data['payback'];
                        $policy["payback_discount"] = $data['paybackDiscount'];
                        //$policy["category_id"] = $data['cid'];
                        $policy["is_auxiliary"] = 1;

                        $oldP = Db::name('customer_price_policy')->where([
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => 1,
                        ])->find();


                        if (!empty($oldP)) {
                            //删除中间表
                            Db::name('policy_goods')->where([
                                "policy_id" => $oldP["id"]
                            ])->delete(true);


                            //删除政策
                            Db::name('customer_price_policy')->where([
                                "customer_number" => $customer["customer_number"],
                                "is_auxiliary" => 1,
                            ])->delete(true);
                        }


                        $policyId = Db::name('customer_price_policy')->insertGetId($policy);

                        $policyGoods = [];
                        foreach ($goods as $good) {
                            $pd = [
                                "policy_id" => $policyId,
                                "goods_number" => $good["goods_number"],
                                "customer_number" => $customer["customer_number"],
                                "is_auxiliary" => 1,
                            ];
                            array_push($policyGoods, $pd);
                        }
                        $res = Db::name('policy_goods')->insertAll($policyGoods);
                    }
                    // 提交事务
                    Db::commit();
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                }
            } elseif ($data["all"] == 1) { //全不选


                foreach ($customers as $customer) {

                    $oldP = Db::name('customer_price_policy')->where([
                        "customer_number" => $customer["customer_number"],
                        "is_auxiliary" => 1,
                    ])->find();


                    if (!empty($oldP)) {
                        //删除中间表
                        Db::name('policy_goods')->where([
                            "policy_id" => $oldP["id"]
                        ])->delete(true);


                        //删除政策
                        Db::name('customer_price_policy')->where([
                            "customer_number" => $customer["customer_number"],
                            "is_auxiliary" => 1
                        ])->delete(true);
                    }
                }
            }
        }
        $res = array(
            "errcode" => "1",
            "errmsg" => "执行成功"
        );
        return json($res);
    }

    public function changeStatus()
    {
        $data = request()->put();
        $status = $data["status"] ? 1 : 0;
        $customers = Db::name("customer")->where("id={$data['customer']['id']} or path like '{$data['customer']['path']}%'")->select();
        $cnumbers = [];
        foreach ($customers as $c) {
            array_push($cnumbers, $c["customer_number"]);
        }
        Db::name('customer_price_policy')->where([
            "customer_number" => $cnumbers,
        ])->update([
            "status" => $status
        ]);


        $res = array(
            "errcode" => "1",
            "errmsg" => "执行成功"
        );
        return json($res);
    }


    public function deleteByCustomerId()
    {
        $data = request()->put();
        $customers = Db::name("customer")->where("id={$data['customer']['id']} or path like '{$data['customer']['path']}%'")->select();
        $cnumbers = [];
        foreach ($customers as $c) {
            array_push($cnumbers, $c["customer_number"]);
        }

        Db::startTrans();
        try {
            Db::name('customer_price_policy')->where([
                "customer_number" => $cnumbers,
            ])->delete(true);
            Db::name('policy_goods')->where([
                "customer_number" => $cnumbers,
            ])->delete(true);
            Db::name('policy_special')->where([
                "customer_number" => $cnumbers,
            ])->delete(true);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
        }


        $res = array(
            "errcode" => "1",
            "errmsg" => "执行成功"
        );
        return json($res);
    }


    public function getPolicyByCustomerNumber()
    {
        $customerNumber = input("customerNumber");
        $policies = Db::name("customer_price_policy")->where("customer_number={$customerNumber}")->select();
        return json($policies);
    }

    public function export()
    {
        $page = input('page');
        $rows = input('rows');

        if (empty($page)) $page = 1;
        if (empty($rows)) $rows = 10;
        $list = Db::name("vw_policy")->order("goods_number asc");
        $list->where("standard_price != 0");

        $brand_id = input("brandId");
        if (!empty($brand_id) && $brand_id != -1) {
            $list->where([
                "brand_id" => $brand_id,
            ]);
        }

        $cidsStr = input("cids");
        if (!empty($cidsStr)) {
            $cids = explode(",", $cidsStr);
            $list->where([
                "category_id" => $cids
            ]);
        }

        //$list->where("customer_number='101010027'");

        $customer_id = input("customerNumber");
        if (!empty($customer_id)) {
            $list->where("customer_number='{$customer_id}'");
        }

        /*$customer_name = input("customerName");
        if (!empty($customer_name)) {
            $list->where("customer_name='{$customer_name}'");
        }*/

        $goods_number = input("goodsNumber");
        if (!empty($goods_number)) {
            $list->where("goods_number='{$goods_number}'");
        }

        /*$goods_name = input("goodsName");
        if (!empty($goods_name)) {
            $list->where("goods_name='{$goods_name}'");
        }*/

        $only = input("only");
        if (!empty($only) && $only == "true") {
            $list->where("special_price is not null");
        }


        //$count = $list->count();
        $list = $list->select();//获取列表

        //var_dump($list);
        //exit();

        $outfile = "价格政策.xlsx";
        $inputFileName = "/opt/客户价格本导入模板.xlsx";
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (\Exception $e) {
            die('加载文件发生错误："' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        };

        //这里引入PHPExcel文件注意路径修改
        //$objExcel = new \PHPExcel();
        //header('Content-Type: application/vnd.ms-excel');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename={$outfile}");
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        //$objWriter = \PHPExcel_IOFactory::createWriter($objExcel, 'Excel2007');
        ob_end_clean();
        $objActSheet = $objPHPExcel->getActiveSheet();
        /*$letter = explode(',', "A,B,C,D,E,F,G,H,I,J,K,L");
        $arrHeader = array('客户编码', '客户','客户简称','存货编码','存货名称','计量单位','加价率%','最低售价','协议价','协议价折扣%','成交价','备注');
        $lenth = count($arrHeader);
        for ($i = 0; $i < $lenth; $i++) {
            $objActSheet->setCellValue("$letter[$i]1", "$arrHeader[$i]");
        };*/
        foreach ($list as $k => $v) {
            $k += 2;
            $objActSheet->setCellValue('A' . $k, $v['customer_number']);
            $objActSheet->setCellValue('B' . $k, $v['customer_name']);
            $objActSheet->setCellValue('C' . $k, $v['customer_name']);
            $objActSheet->setCellValue('D' . $k, $v['goods_number']);
            $objActSheet->setCellValue('E' . $k, $v['goods_name']);
            $objActSheet->setCellValue('F' . $k, $v['unit_name']);
            $objActSheet->setCellValue('G' . $k, '');
            $objActSheet->setCellValue('H' . $k, '');
            $objActSheet->setCellValue('I' . $k, $v['standard_price']);
            $discount = null;
            $dealing_price = null;
            $desc="";
            if (!empty($v['special_price'])) {
                $dealing_price = $v['special_price'];
                if ($v['standard_price'] == 0) {
                    $discount = 0;
                } else {
                    $discount = sprintf("%.4f",$v['special_price'] / $v['standard_price']);
                    if(sprintf("%.2f",$v['standard_price']*$discount)!=$v['special_price']){
                        $desc="请手动修改";
                    }
                }

            } elseif (empty($v['special_price']) && !empty($v['brand_discount'])) {
                $discount = (100 - $v['brand_discount']) / 100;
                $dealing_price = $v['dealing_price'];
            } elseif (empty($v['special_price']) && empty($v['brand_discount']) && !empty($v['auxiliary_discount'])) {
                $discount = (100 - $v['auxiliary_discount']) / 100;
                $dealing_price = $v['dealing_price'];
            } else {
                $dealing_price = $v['standard_price'];
            }
            $objActSheet->setCellValue('J' . $k, $discount);
            $objActSheet->setCellValue('K' . $k, $dealing_price);
            $objActSheet->setCellValue('L' . $k, $desc);
        }
        //$width = array(20, 20, 15, 10, 10, 30, 10, 15);
        //设置表格的宽度
        //$objActSheet->getColumnDimension('A')->setWidth($width[0]);
        //$objActSheet->getColumnDimension('B')->setWidth($width[1]);


        /* header("Content-Type: application/vnd.ms-excel");
         header("Content-Type: application/force-download");
         //header("Content-Type: application/octet-stream");

         header("Content-Type: application/download");
         header('Content-Disposition:inline;filename="' . $outfile . '"');
         header("Content-Transfer-Encoding: binary");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Pragma: no-cache");*/
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');    //这里直接导出文件

    }

    public function test()
    {
        return $this->fetch();
    }

    public function page_list()
    {
        $page = input('page');
        $rows = input('rows');
        $brand_id = input("brandId");
        $customer_id = input("cn");
        $cidsStr = input("cids");
        $auxiliary = input("auxiliary");

        if (empty($page)) $page = 1;
        if (empty($rows)) $rows = 10;
        $list = Db::name("goods")
            ->alias("g")
            ->field("g.goods_number AS goods_number,
	g.goods_name AS goods_name,
	g.goods_specification AS goods_specification,
	NULL AS customer_number,
	NULL AS customer_name,
	g.standard_price AS standard_price,
	g.brand_id AS brand_id,
	g.category_id AS category_id,
	g.is_auxiliary AS is_auxiliary,
	NULL AS brand_discount,
	NULL AS auxiliary_discount,
	NULL AS brand_payback_discount,
	NULL AS auxiliary_payback_discount,
	NULL AS special_price,
	NULL AS dealing_price")
            ->order("goods_number asc");

        if (!empty($brand_id)) {
            $list->where([
                "brand_id" => $brand_id,
            ]);
        }

        if (!empty($cidsStr)) {
            $cids = explode(",", $cidsStr);
            $list->where([
                "category_id" => $cids
            ]);
        }


        if (is_numeric($auxiliary) && $auxiliary != 0) {
            $list->where("is_auxiliary='{$auxiliary}'");
        }


        $count = $list->count();
        $list = $list->page($page, $rows)->select();//获取列表


        if ($auxiliary == 0) {
            $existList = Db::name("vw_policy")
                ->order("goods_number asc")
                ->where("customer_number='{$customer_id}' and brand_id = {$brand_id}")->select();
        } else {
            $existList = Db::name("vw_policy")
                ->order("goods_number asc")
                ->where("customer_number='{$customer_id}' and is_auxiliary=1")->select();
        }

        $res = array(
            'total' => $count,
            'rows' => $list,
            'existRows' => $existList,
        );
        return json($res);
    }


    public function get()
    {
        $customer_number = input("customer_number");
        $goods_number = input("goods_number");
        $policy = Db::name("vw_policy")->where([
            "customer_number" => $customer_number,
            "goods_number" => $goods_number,
        ])->find();
        return json($policy);

    }


    public function getP()
    {
        $customer_number = input("customer_number");
        $auxiliary = input("auxiliary");
        $brandId = input("brandId");

        $policy = Db::name("customer_price_policy");

        if (!empty($customer_number)) {
            $policy->where([
                "customer_number" => $customer_number,
            ]);
        }

        if (!empty($auxiliary)) {
            $policy->where([
                "is_auxiliary" => $auxiliary,
            ]);
        };

        if (!empty($brandId)) {
            $policy->where([
                "brand_id" => $brandId,
            ]);
        };

        $policy = $policy->find();

        return json($policy);

    }

    public function page_list_manage()
    {
        $page = input('page');
        $rows = input('rows');

        if (empty($page)) $page = 1;
        if (empty($rows)) $rows = 10;
        $list = Db::name("vw_policy")->order("goods_number asc");

        $brand_id = input("brandId");
        if (!empty($brand_id) && $brand_id != -1) {
            $list->where([
                "brand_id" => $brand_id,
            ]);
        }

        $cidsStr = input("cids");
        if (!empty($cidsStr)) {
            $cids = explode(",", $cidsStr);
            $list->where([
                "category_id" => $cids
            ]);
        }

        $customer_id = input("customerNumber");
        if (!empty($customer_id)) {
            $list->where("customer_number='{$customer_id}'");
        }

        $customer_name = input("customerName");
        if (!empty($customer_id)) {
            $list->where("customer_name='{$customer_name}'");
        }



        /*$customer_name = input("customerName");
        if (!empty($customer_name)) {
            $list->where("customer_name='{$customer_name}'");
        }*/

        $goods_number = input("goodsNumber");
        if (!empty($goods_number)) {
            $list->where("goods_number='{$goods_number}'");
        }

        $goods_name = input("goodsName");
        if (!empty($goods_name)) {
            $list->where("goods_name='{$goods_name}'");
        }

        $only = input("only");
        if (!empty($only) && $only == "true") {
            $list->where("special_price is not null");
        }


        $count = $list->count();
        $list = $list->page($page, $rows)->select();//获取列表
        $res = array(
            'total' => $count,
            'rows' => $list
        );
        return json($res);
    }

    public function update()
    {
        $goods_number = input("goods_number");
        $customer_number = input("customer_number");
        $standard_price = input("standard_price");
        $brand_discount = input("brand_discount");
        $brand_payback_discount = input("brand_payback_discount");
        $auxiliary_discount = input("auxiliary_discount");
        $auxiliary_payback_discount = input("auxiliary_payback_discount");
        $special_price = input("special_price");


        /*Db::startTrans();
        try {


            Db::name("goods")
                        ->where('goods_number', $goods_number)
                        ->update([
                            'standard_price' => empty($standard_price)?0:$standard_price
                        ]);



            Db::name("customer_price_policy")
                ->where([
                    'goods_number'=> $goods_number,
                    'customer_number'=> $customer_number,
                    'is_auxiliary'=> 0,
                ])
                ->update([
                    'discount' => empty($brand_discount)?null:$brand_discount,
                    'payback_discount' => empty($brand_payback_discount)?null:$brand_payback_discount,
                    'special_price' => empty($special_price)?null:$special_price,
                ]);


            Db::name("customer_price_policy")
                ->where([
                    'goods_number'=> $goods_number,
                    'customer_number'=> $customer_number,
                    'is_auxiliary'=> 1,
                ])
                ->update([
                    'discount' => empty($auxiliary_discount)?null:$auxiliary_discount,
                    'payback_discount' => empty($auxiliary_payback_discount)?null:$auxiliary_payback_discount,
                    'special_price' => empty($special_price)?null:$special_price,
                ]);



                // 提交事务
                Db::commit();

        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
        }*/


        $pd = Db::name("policy_special")->where([
            "goods_number" => $goods_number,
            "customer_number" => $customer_number,
        ])->find();


        if (empty($pd)) {
            Db::name("policy_special")->insert([
                "goods_number" => $goods_number,
                "customer_number" => $customer_number,
                "special_price" => $special_price
            ]);
        } else {
            Db::name("policy_special")->where([
                "goods_number" => $goods_number,
                "customer_number" => $customer_number,
            ])->update([
                "special_price" => $special_price
            ]);
        }

        $res = array(
            'errcode' => 0,
            'errmsg' => "ok"
        );

        return json($res);
    }


    public function add()
    {
        $goods_number = input("goods_number");
        $customer_number = input("customer_number");
        //$standard_price=input("standard_price");
        $brand_discount = input("brand_discount");
        $brand_payback_discount = input("brand_payback_discount");
        $auxiliary_discount = input("auxiliary_discount");
        $auxiliary_payback_discount = input("auxiliary_payback_discount");
        $special_price = input("special_price");

        $pd = Db::name("policy_special")->where([
            "goods_number" => $goods_number,
            "customer_number" => $customer_number,
        ])->find();


        if (empty($pd)) {
            Db::name("policy_special")->insert([
                "goods_number" => $goods_number,
                "customer_number" => $customer_number,
                "special_price" => $special_price
            ]);
        } else {
            Db::name("policy_special")->where([
                "goods_number" => $goods_number,
                "customer_number" => $customer_number,
            ])->update([
                "special_price" => $special_price
            ]);
        }

        $res = array(
            'errcode' => 0,
            'errmsg' => "ok"
        );

        return json($res);
    }
}