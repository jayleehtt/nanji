<?php
/**
 * Created by PhpStorm.
 * User: VSZ_005
 * Date: 2020/6/5
 * Time: 16:51
 */

namespace app\admin\controller;


use app\core\controller\BaseCtrl;
use app\admin\model\GoodsCategory as GoodsCategoryModel;
use think\Db;

/**
 * 商品分类管理
 * Class GoodsCategory
 * @package app\admin\controller
 */
class GoodsCategory extends BaseCtrl
{

    public function tree($num,$level) {
        //$category = GoodsCategoryModel::all();
        $category = Db::query("SELECT t1.id,t1.parent_id,t1.category_number,t1.tree_names,t1.tree_path,t1.category_name,t2.brand_id as brand_id FROM fo_goods_category t1 LEFT JOIN fo_category_brand t2 on t1.id=t2.category_id order by t1.category_number asc");
        $arrs = [];
        $tree = recur($arrs,$category,0,$num,$level);
        return json($tree);
    }


    public function page()
    {
        $brand = new BrandModel;
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $list=$brand->order('id')->page($page,$rows)->select();
        $total = $brand->count();
        $res=array(
            'total'=>$total,
            'rows'=>$list
        );
        return json($res);
    }

    public function getCateByBrandId(){
        $brandId=input("brand_id");
        $num=input("num");
        $level=input("level");
        $m=new GoodsCategoryModel();
        $categories = $m->getCateByBrandId($brandId);
        if(count($categories)>0){
            $arrs = [];
            $tree = recur($arrs,$categories,0,$num,$level);
            return json($tree);
        }else{
            return json([]);
        }

    }
}