<?php
/**
 * Created by PhpStorm.
 * User: VSZ_005
 * Date: 2020/6/4
 * Time: 17:37
 */

namespace app\admin\controller;
use app\core\controller\BaseCtrl;
use think\Db;

class Customer extends BaseCtrl
{
    public function page()
    {
        return $this->fetch();
    }

    public function likeCumtoerNumberOrCustomerName()
    {
        $customerNumber = input("customer_id");
        $customerName = input("customer_name");
        $auxiliary = input("auxiliary");
        $brandId = input("brandId");

        $list = Db::name("customer")
            ->alias("c");
           // ->field("c.id,c.customer_name,c.customer_number,c.parent_id,c.path,cpp.`status`,cpp.discount,cpp.payback,cpp.is_auxiliary,cpp.payback_discount,cpp.brand_id")
            //->leftJoin("customer_price_policy cpp","c.customer_number=cpp.customer_number");

        if(!empty($customerNumber)){
            //$where="customer_number like '%{$customerNumber}%'";
            $list->where("c.customer_number like '%{$customerNumber}%'");
        }
        if(!empty($customerName)){
            //$where="customer_name like '%{$customerName}%'";
            $list->where("c.customer_name like '%{$customerName}%'");
        }
        if(!empty($auxiliary)){
            //$where="customer_name like '%{$customerName}%'";
            $list->where("cpp.is_auxiliary={$auxiliary}");
        }

        if(!empty($brandId)){
            $list->where("cpp.brand_id={$brandId}");
        }


        //$customerList = Db::name("customer")->where($where)->select();
        $customerList = $list->select();
        $res=array(
            'total'=>0,
            'rows'=>$customerList
        );
        return json($res);
    }

    public function pageList()
    {
        $pageNum = input('page');
        $rows = input('rows');
        if(empty($pageNum)) $pageNum=1;
        if(empty($rows)) $rows=10;
        $page = Db::name("customer")->page($pageNum,$rows)->select();
        $total = Db::name("customer")->count();
        $res=array(
            'total'=>$total,
            'rows'=>$page
        );
        return json($res);
    }

}