<?php
/**
 * Created by PhpStorm.
 * User: VSZ_005
 * Date: 2020/6/5
 * Time: 16:51
 */

namespace app\admin\controller;


use app\core\controller\BaseCtrl;
use app\admin\model\Brand as BrandModel;
use think\Db;
use think\Request;
/**
 * 品牌管理
 * Class Brand
 * @package app\admin\controller
 */
class Brand extends BaseCtrl
{

    public function table()
    {
        return $this->fetch();
    }

    public function get($id)
    {
        $brand = new BrandModel;
        $ret = $brand->get($id);
        $res=array(
            "brandName"=>$ret["brand_name"],
            "id"=>$ret["id"],
        );
        return json($res);
    }

    public function create(Request $request)
    {
        $brand = new BrandModel;
        $ret = $brand->save([
            'brand_name' => $request->param("brandName")
        ]);
        $res=array(
            'errcode'=>0,
            'errmsg'=>"ok"
        );
        if($ret){
            return json($res);
        }else{
            $res["errcode"]=40000;
            $res["errmsg"]="新增失败";
            return json($res);
        }
    }


    public function update(Request $request)
    {
        $brand = new BrandModel;
        $id=$request->param("id");
        $ret = $brand->save(
            [
            'brand_name' => $request->param("brandName")
            ],
            ['id' => $id]
        );
        $res=array(
            'errcode'=>0,
            'errmsg'=>"ok"
        );
        if($ret){
            return json($res);
        }else{
            $res["errcode"]=40000;
            $res["errmsg"]="新增失败";
            return json($res);
        }
    }

    public function remove($ids)
    {
        $brand = new BrandModel;
        $ret = $brand->destroy($ids);
        $res=array(
            'errcode'=>0,
            'errmsg'=>"ok"
        );
        if($ret){
            return json($res);
        }else{
            $res["errcode"]=40000;
            $res["errmsg"]="删除失败";
            return json($res);
        }
    }


    public function pageList($page=1,$rows=10)
    {
        $brand = new BrandModel;
        $list=$brand->order('update_at desc')->page($page,$rows)->select();
        $total = $brand->count();
        $res=array(
            'total'=>$total,
            'rows'=>$list
        );
        return json($res);
    }


    public function  getByIsDiscount(){
        $brands = Db::name("brand")->where("is_discount=1")->order('sort','desc')->select();
        return json($brands);
    }

}