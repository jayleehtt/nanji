<?php
/**
 * Created by PhpStorm.
 * User: VSZ_005
 * Date: 2020/6/5
 * Time: 16:50
 */

namespace app\core\model;


use think\Model;

class BaseModel extends Model
{
    protected $auto = [];
    protected $insert = ['create_at','update_at','create_by','update_by'];
    protected $update = ['update_at','update_by'];

    protected function setCreateAtAttr()
    {
        return date('Y-m-d H:i:s');
    }

    protected function setUpdateAtAttr()
    {
        return date('Y-m-d H:i:s');
    }

    protected function setCreateByAttr()
    {
        return 1;
    }


    protected function setUpdateByAttr()
    {
        return 1;
    }


}