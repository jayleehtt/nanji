<?php
namespace app\index\controller;
use app\core\controller\BaseCtrl;

class Index extends BaseCtrl
{
    public function index()
    {
        return $this->fetch('main');
    }

    public function test()
    {
        $num=(4444.44/5152);
        $r=sprintf("%.4f",$num);
        echo $r;
    }
}
